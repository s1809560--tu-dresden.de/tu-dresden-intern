#!/bin/bash

set -eu

echo "sudo wird für die Paket-Installation benötigt, ggf. separat ausführen"
echo "sudo required for package installations, alternatively run separately"

PREFIX="${1:-$HOME/local}"

sudo apt-get install \
    cmake extra-cmake-modules g++ gettext git kirigami2-dev libboost-container-dev \
    libcairo2-dev libchm-dev libcurl4-nss-dev libdjvulibre-dev libepub-dev \
    libfontconfig1-dev libfreetype6-dev \
    libkf5activities-dev libkf5archive-dev libkf5bookmarks-dev libkf5completion-dev \
    libkf5config-dev libkf5coreaddons-dev libkf5crash-dev libkf5dbusaddons-dev \
    libkf5doctools-dev libkf5i18n-dev libkf5kexiv2-dev libkf5khtml-dev libkf5kio-dev \
    libkf5kjs-dev libkf5notifications-dev libkf5parts-dev libkf5pty-dev libkf5purpose-dev \
    libkf5threadweaver-dev libkf5wallet-dev \
    liblcms2-dev libnss3-dev libopenjp2-7-dev libopenjpip-dec-server libphonon4qt5-dev \
    libpng-dev libpng++-dev libqmobipocket-dev libqt5svg5-dev libqt5texttospeech5-dev \
    libspectre-dev libtiff5-dev libzip-dev pkg-config qtbase5-dev qtdeclarative5-dev 

BUILD_DIR="$(mktemp -d)"

## build poppler

cd "$BUILD_DIR"

git clone https://gitlab.freedesktop.org/poppler/poppler.git
cd poppler

mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX="$PREFIX" -DCMAKE_BUILD_TYPE=Release ..

make -j$(nproc)
make install

## make okular

cd "$BUILD_DIR"

git clone https://invent.kde.org/graphics/okular.git
cd okular

mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX="$PREFIX" -DCMAKE_PREFIX_PATH="$PREFIX" -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc)
make install


## make new okular more convenient to run

mv "$PREFIX/bin/okular" "$PREFIX/bin/okular_"

echo "#!/bin/bash
export OKULARPREFIX=\"$PREFIX\"
export PATH=\"\$OKULARPREFIX/bin:\$PATH\"
export LD_LIBRARY_PATH=\"\$OKULARPREFIX/lib/x86_64-linux-gnu:\$OKULARPREFIX/lib/:\$LD_LIBRARY_PATH\"
export QT_PLUGIN_PATH=\"\$OKULARPREFIX/lib/x86_64-linux-gnu/plugins:\$QT_PLUGIN_PATH\"
export QML2_IMPORT_PATH=\"\$OKULARPREFIX/lib/x86_64-linux-gnu/qml:\$QML2_IMPORT_PATH\"
export QT_QUICK_CONTROLS_STYLE_PATH=\"\$OKULARPREFIX/lib/x86_64-linux-gnu/qml/QtQuick/Controls.2/:\$QT_QUICK_CONTROLS_STYLE_PATH\"
$PREFIX/bin/okular_" >"$PREFIX/bin/okular"

chmod +x "$PREFIX/bin/okular"

echo "Bitte $PREFIX/bin in die PATH-Variable aufnehmen / please add $PREFIX/bin to your PATH variable"


